import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { RegisteredActivitiyListComponent } from './registered-activitiy-list/registered-activitiy-list.component';
import { ActivityRegistrationComponent } from './activity-registration/activity-registration.component';
import { LoginFormComponent } from '../shared/login-form/login-form.component';
import { StudentsViewComponent } from '../students/view/students.view.component';
import { PendingStudentListComponent } from './pending-student-list/pending-student-list.component';




const AdminRoutes: Routes = [
    { path: 'detail/:id', component: StudentsViewComponent },
 
    {
        path: 'admin', 
        component: AdminNavComponent,
        children : [
            { path : '', redirectTo : 'activities', pathMatch: 'full'},
            { path : 'registered', component : RegisteredActivitiyListComponent},
            { path : 'activities', component : ActivityRegistrationComponent},
            { path: 'pending', component: PendingStudentListComponent }
        ]
 },
    { path: 'login', component: LoginFormComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {
    
}
