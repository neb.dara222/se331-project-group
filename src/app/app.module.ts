import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatExpansionModule, MatBadgeModule
} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import {MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentsRestImplService } from './service/students-rest-impl.service';
import { ActivityService } from './service/activity-service';
import { ActivityRestImplService } from './service/activity-rest-impl.service';
import { LoginFormComponent } from './shared/login-form/login-form.component';
import { UserComponent } from './user/user.component';
import { RegisterFormComponent } from './shared/register-form/register-form.component';
import { StudentNavComponent } from './students/student-nav/student-nav.component';
import { StudentActivityComponent } from './students/student-activity/student-activity.component';
import { ActivityRegistrationComponent } from './admin/activity-registration/activity-registration.component';
import { RegisteredActivitiyListComponent } from './admin/registered-activitiy-list/registered-activitiy-list.component';
import { PendingStudentListComponent } from './admin/pending-student-list/pending-student-list.component';
import { AdminNavComponent } from './admin/admin-nav/admin-nav.component';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { ActivityListComponent } from './teacher/activity-list/activity-list.component';
import { StudentListComponent } from './teacher/student-list/student-list.component';
import { TeacherNavComponent } from './teacher/teacher-nav/teacher-nav.component';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { ActivityDetailComponent } from './students/activity-detail/activity-detail.component';
import { EnrolledTableComponent } from './students/enrolled-table/enrolled-table.component';
import { TeacherWelcomeComponent } from './teacher/teacher-welcome/teacher-welcome.component';
import { StudentPendingComponent } from './teacher/student-pending/student-pending.component';
import { StudentProfileComponent } from './students/student-profile/student-profile.component';
import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';


@NgModule({
  declarations: [
    AppComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    LoginFormComponent,
    UserComponent,
    RegisterFormComponent,
    TeacherNavComponent,
    ActivityListComponent,
    StudentListComponent,
    StudentNavComponent,
    StudentActivityComponent,
    ActivityRegistrationComponent,
    RegisteredActivitiyListComponent,
    PendingStudentListComponent,
    AdminNavComponent,
    ActivityListComponent,
    StudentListComponent,
    TeacherNavComponent,
    ActivityDetailComponent,
    EnrolledTableComponent,
    TeacherWelcomeComponent,
    StudentPendingComponent,
    StudentProfileComponent,
    EditActivityComponent,
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    StudentRoutingModule,
    AdminRoutingModule,
    TeacherRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatExpansionModule,
    MatBadgeModule,
    FlexLayoutModule
    
  ],
  providers: [
    { provide: StudentService, useClass: StudentsRestImplService },
    { provide: ActivityService, useClass: ActivityRestImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
