import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import Activity from '../entity/activity';
import { Observable } from '../../../node_modules/rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService extends ActivityService{
  constructor(private http: HttpClient) { 
    super()
  }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityApi);
  }

  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity>(environment.activityApi+"/" + id );
  }

  saveActivity(activity: Activity): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi,activity);
  }
  enrollActivity(activity: Activity): Observable<Activity> {
    return this.http.post<Activity>(environment.EnrollmentApi,activity);
  }
  getEnrollActivity(): Observable<Activity[]>{
    return this.http.get<Activity[]>(environment.EnrollmentApi);
  };
  updateActivity(id: number,activity: Activity): Observable<Activity> {
    return this.http.put<Activity>(environment.activityApi+"/"+id,activity);
  }

  

}
