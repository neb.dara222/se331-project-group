import Activity from '../entity/activity';
import { Observable } from '../../../node_modules/rxjs';

export abstract class ActivityService {
     abstract getActivities(): Observable<Activity[]>;
     abstract getActivity(id: number): Observable<Activity>;
     abstract saveActivity(activity: Activity): Observable<Activity>;
     abstract enrollActivity(activity: Activity): Observable<Activity>;
     abstract getEnrollActivity(): Observable<Activity[]>;
     abstract updateActivity(id: number,activity: Activity): Observable<Activity>;
}
