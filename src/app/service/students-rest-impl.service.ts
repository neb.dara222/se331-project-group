import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentsRestImplService extends StudentService{

  constructor(private http: HttpClient) { 
    super()
  }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }s

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi+"/" + id );
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi,student);
  }
  updateStudent(student: Student,id : number): Observable<Student> {
    return this.http.put<Student>(environment.studentApi+"/"+id,student);
  }
  enrollStudent(id: number,student: Student,isPending: boolean): Observable<Student> {
    return this.http.put<Student>(environment.studentApi+"/"+id,student);
  }
  rejectStudent(id: number,student: Student,isPending: boolean): Observable<Student> {
    return this.http.put<Student>(environment.studentApi+"/"+id,student);
  }
  
 
}
