import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private router: Router) { }

  Email: string;
  password: string;
  role: string;

  login() : void {
    console.log(this.Email);
    console.log(this.password);
    console.log(this.role)

    if(this.Email == 'admin@cmu.ac.th' && this.password == 'admin' && this.role == 'admin'){
      
     this.router.navigate(["admin"]);
    } 
    else if (this.Email == 'teacher@cmu.ac.th' && this.password == 'teacher' && this.role == 'teacher') {
      this.router.navigate(["teacher"]);
    } 
    else if (this.Email == 'student@cmu.ac.th' && this.password == 'student' && this.role == 'student') {
      this.router.navigate(["student"]);
    } else {
      alert("Invalid Username or Password");
    }

  }
  ngOnInit() {

  }

}
