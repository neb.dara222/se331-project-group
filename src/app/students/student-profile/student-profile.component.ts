import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/student-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import Student from 'src/app/entity/student';

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.css']
})
export class StudentProfileComponent implements OnInit {

  form = this.fb.group({
    id: [''],
    studentEmail: [''],
    studentFirstname: [''],
    studentSurname: [''],
    studentDob: [''],
    studentId: [''],
    studentPassword: ['']

  });
  
  student : Student;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private studentService: StudentService,private router: Router) { }

  submit() {
    this.studentService.updateStudent(this.form.value,this.student.id)
    .subscribe(() => {
      this.router.navigate(['./student']);
    }, (error)=> {
      alert('could not save the value')
    }
    )
  }
  update(){
    alert("Update information succesfully!!");
  }

  ngOnInit() {
    this.route.params
     .subscribe((params: Params) => {
     this.studentService.getStudent(+params['id'])
     .subscribe((inputStudent: Student) => this.student = inputStudent);
     });
    
  }

}
