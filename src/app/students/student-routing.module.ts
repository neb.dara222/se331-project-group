import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { LoginFormComponent } from '../shared/login-form/login-form.component';
import { StudentNavComponent } from './student-nav/student-nav.component';
import { StudentActivityComponent } from './student-activity/student-activity.component';

import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { EnrolledTableComponent } from './enrolled-table/enrolled-table.component';
import { StudentsViewComponent } from './view/students.view.component';


const StudentRoutes: Routes = [
   
    {
        path: 'student', 
        component: StudentNavComponent,
        children : [
            { path : '', redirectTo : 'activity', pathMatch: 'full'},
            { path : 'activity', component : StudentActivityComponent},
            { path : 'activity-detail/:id' , component : ActivityDetailComponent},
            { path : 'enrolled', component : EnrolledTableComponent},
            { path : 'profile/:id', component : StudentsViewComponent }
        ]
 },
    { path: 'login', component: LoginFormComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
