import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from 'src/app/service/student-service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-students-view',
  templateUrl: './students.view.component.html',
  styleUrls: ['./students.view.component.css']
})
export class StudentsViewComponent implements OnInit{
  student:Student;
  students: Student[];
  constructor(private fb: FormBuilder,private route: ActivatedRoute, private studentService: StudentService){}

  form = this.fb.group({
    id: [''],
    studentEmail: [''],
    studentFirstname: [''],
    studentSurname: [''],
    studentDob: [''],
    studentId: [''],
    studentPassword: ['']
     })

  ngOnInit(): void {
  
    this.route.params
     .subscribe((params: Params) => {
     this.studentService.getStudent(+params['id'])
     .subscribe((inputStudent: Student) => this.student = inputStudent);
     });
    
  }
  update(value : Student){
    this.studentService.updateStudent(this.form.value,this.student.id)
    .subscribe((student) => {
      alert("success to update "+ student.studentFirstname);
      // this.router.navigate(['./student']);
    }, (error)=> {
      alert('could not save the value')
    }
    )
  }
}
 