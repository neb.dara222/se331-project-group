import { Component, OnInit } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { FormBuilder } from '@angular/forms';
import { ActivityService } from 'src/app/service/activity-service';
import { Router, ActivatedRoute, Params  } from '@angular/router';

@Component({
  selector: 'app-edit-activity',
  templateUrl: './edit-activity.component.html',
  styleUrls: ['./edit-activity.component.css']
})
export class EditActivityComponent implements OnInit {
  // activities: Activity[];
  activity: Activity;

  form = this.fb.group({
    activityName: [''],
    activityTime: [''],
    activityDate: [''],
    activityLocation: [''],
    periodRegistration: [''],
    activityDescription: [''],
    hostTeacher: [''],
    id: [''],
    Status : ['Pending']
  })

  constructor(private fb: FormBuilder, private activityService: ActivityService, private router: Router, private route: ActivatedRoute) { }

  submit() {
    
    if(confirm("Are you sure to confirm?")) { 
      this.activityService.updateActivity(this.activity.id,this.form.value);
      // .subscribe(() => {
      //   this.router.navigate(['./teacher/activitylist']);
      // }, (error)=> {
      //   alert('could not save the value' + error)
      // }
      // )
    } else {
      alert("Oops, could not confirm the student !!!")
    }
  }

  update(){
    alert("Update information succesfully!!");
  }

  ngOnInit() {

    this.route.params
     .subscribe((params: Params) => {
     this.activityService.getActivity(+params['id'])
     .subscribe((inputActivity: Activity) => this.activity = inputActivity);
     });
     
    
  }

}

