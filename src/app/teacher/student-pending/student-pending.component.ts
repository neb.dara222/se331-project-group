import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {StudentPendingDataSource} from './student-pending.datasource';

@Component({
  selector: 'app-student-pending',
  templateUrl: './student-pending.component.html',
  styleUrls: ['./student-pending.component.css']
})
export class StudentPendingComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentPendingDataSource;

  displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'approve'];
  students: Student[];
  student : Student;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private studentService: StudentService,private router: Router) { }

  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(students => {
        this.dataSource = new StudentPendingDataSource();
        this.dataSource.data = students;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  Approve(student: Student){
    console.log(student);
    if(confirm("Are you sure to APPROVE "+student.studentFirstname+" to the activity")) {
    //   this.studentService.enrollStudent(student.id, true)
    //   .subscribe(() => {
    //   this.router.navigate(['./pendinglist'])
    // }, (error)=> {
    //   alert('could not save the value' + error)
    // })
     }
  }

  Reject(student: Student){
    console.log(student);
    if(confirm("Are you sure to REJECT "+student.studentFirstname+" to the activity")) {
    //   this.studentService.rejectStudent(student.id,false)
    //   .subscribe(() => {
    //   this.router.navigate(['./pendinglist'])
    // }, (error)=> {
    //   alert('could not save the value' + error)
    // })
    }
  }
  
  // averageGpa() {
  //   let sum = 0;
  //   if (Array.isArray(this.students)) {
  //     for (const student of this.students) {
  //       sum += student.gpa;
  //     }
  //     return sum / this.students.length;
  //   } else {
  //     return null;
  //   }

  // }

  // upQuantity(student: Student) {
  //   student.penAmount++;
  // }

  // downQuantity(student: Student) {
  //   if (student.penAmount > 0) {
  //     student.penAmount--;
  //   }
  // }

}
