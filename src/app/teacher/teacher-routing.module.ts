import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginFormComponent } from '../shared/login-form/login-form.component';
import { TeacherNavComponent } from './teacher-nav/teacher-nav.component';
import { StudentListComponent } from './student-list/student-list.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { TeacherWelcomeComponent } from './teacher-welcome/teacher-welcome.component';
import { StudentPendingComponent } from './student-pending/student-pending.component';
import { EditActivityComponent } from './edit-activity/edit-activity.component';


const TeacherRoutes: Routes = [
   
    {
        path: 'teacher', 
        component: TeacherNavComponent ,
        children : [
            { path : '', redirectTo : 'welcome', pathMatch: 'full'},
            { path : 'welcome', component : TeacherWelcomeComponent },
            { path : 'studentlist', component : StudentListComponent },
            { path : 'activitylist', component : ActivityListComponent },
            { path : 'pendinglist', component : StudentPendingComponent },
            { path : 'editactivity/:id', component : EditActivityComponent }
        ]
 },
    { path: 'login', component: LoginFormComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(TeacherRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class TeacherRoutingModule {

}
