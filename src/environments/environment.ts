import Activity from 'src/app/entity/activity';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  activityApi: ' http://localhost:3000/posts',
  // studentRegistration api
  studentApi: ' http://localhost:3000/comments',
  // enrolled activity
  enrolledActivity: 'http://localhost:3000/profile',

  EnrollmentApi: 'http://localhost:3000/Enroll'

};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
